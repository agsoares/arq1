﻿         assume cs:codigo,ds:dados,es:dados,ss:pilha

CR       EQU    0DH ; constante - codigo ASCII do caractere "carriage return"
LF       EQU    0AH ; constante - codigo ASCII do caractere "line feed"

; definicao do segmento de dados do programa
dados    segment


;#################################################################
;########################## Interface ############################
;#################################################################

Interface		db	'   Campo Minado III - por Adriano Guimaraes Soares - 00180247 - Julho de 2010',CR ,LF, ?
			db	'', CR, LF, ?
			db	'     1   2   3   4   5   6   7   8   9   10', CR,  LF, ?
			db	'   *---*---*---*---*---*---*---*---*---*---*', CR, LF, ?
			db	' 1 |   |   |   |   |   |   |   |   |   |   |   Total de minas no campo:   15', CR, LF, ?
			db	'   *---*---*---*---*---*---*---*---*---*---*   Total de minas marcadas:    0', CR, LF, ?
			db	' 2 |   |   |   |   |   |   |   |   |   |   |   Total de minas a marcar:   15', CR, LF, ?
			db	'   *---*---*---*---*---*---*---*---*---*---*', CR, LF, ?
			db	' 3 |   |   |   |   |   |   |   |   |   |   |', CR, LF, ?
			db	'   *---*---*---*---*---*---*---*---*---*---*   Painel de Controle', CR, LF, ?
			db	' 4 |   |   |   |   |   |   |   |   |   |   |', CR, LF, ?
			db	'   *---*---*---*---*---*---*---*---*---*---*   Linha                01', CR, LF, ?
			db	' 5 |   |   |   |   |   |   |   |   |   |   |', CR, LF, ?
			db	'   *---*---*---*---*---*---*---*---*---*---*   Coluna               01', CR, LF, ?
			db	' 6 |   |   |   |   |   |   |   |   |   |   |', CR, LF, ?
			db	'   *---*---*---*---*---*---*---*---*---*---*   Abrir(A)/Marcar(M): [M]', CR, LF, ?
			db	' 7 |   |   |   |   |   |   |   |   |   |   |', CR, LF, ?
			db	'   *---*---*---*---*---*---*---*---*---*---*   > TAB para alternar entre A/M', CR, LF, ?
			db	' 8 |   |   |   |   |   |   |   |   |   |   |   > SETAS para percorrer o campo', CR, LF, ?
			db	'   *---*---*---*---*---*---*---*---*---*---*   > ENTER para (A)brir/(M)arcar', CR, LF, ?
			db	' 9 |   |   |   |   |   |   |   |   |   |   |', CR, LF, ?
			db	'   *---*---*---*---*---*---*---*---*---*---*   Mensagens:', CR, LF, ?
			db	'10 |   |   |   |   |   |   |   |   |   |   |   ................................', ? 
			db	'   *---*---*---*---*---*---*---*---*---*---*   ................................', ?
			db	'', '$'

;#################################################################
;########################### Matrizes ############################
;#################################################################

Prot_1			db 0,0,0,0,0,0,0,0,0,0
			db 0,0,0,0,0,0,0,0,0,0


Campo			db 0,0,0,0,0,0,0,0,0,0
			db 0,0,0,0,0,0,0,0,0,0
			db 0,0,0,0,0,0,0,0,0,0
			db 0,0,0,0,0,0,0,0,0,0
			db 0,0,0,0,0,0,0,0,0,0
			db 0,0,0,0,0,0,0,0,0,0
			db 0,0,0,0,0,0,0,0,0,0
			db 0,0,0,0,0,0,0,0,0,0
			db 0,0,0,0,0,0,0,0,0,0
			db 0,0,0,0,0,0,0,0,0,0


Prot_2			db 0,0,0,0,0,0,0,0,0,0
			db 0,0,0,0,0,0,0,0,0,0
	
;#################################################################
;########################## Variaveis ############################
;#################################################################

CursorC		db 6
CursorL		db 4
CampoL	 	db 1
CampoC		db 1
Cursor_Real dw 0
Status_Tab	db 'M'
Pra_Marcar	db 15
Pra_Abrir	db 85
Minas		db 15
Temp		dw 0
TEMP_X		db 0
TEMP_Y		db 0
TEMP_CORD	db 0
DEZ			db 10

;#################################################################
;########################## Mensagens ############################
;#################################################################
_Um			db '01','$'
_Dez        db '10','$'
_Ganhou		db 'Ganhou','$'
_Perdeu		db 'You Lose... Shame on you u.u    ','$'
_Retry?		db 'Retry??! (s/n)                  ','$' ;65

;---------------------------------------------------------*
; Dados para a funcao RAND - copiar p/fim segmento 'dados'|
;---------------------------------------------------------*
; dados para 4 partidas, com sorteios duplos nas 2 1as   ;|
_@RNDvet label byte ; 8 minas por linha                  ;|
                                                         ;|
     DB  1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8  ;|
     DB  9, 9,10,10,10, 1, 9, 2, 4, 4, 1, 9, 1,10, 2,10  ;|
                                                         ;|
     DB  2, 2, 2, 3, 2, 4, 2, 5, 2, 6, 3, 2, 3, 4, 3, 6  ;|
     DB  4, 2, 4, 3, 4, 4, 4, 5, 4, 6, 2, 5, 5, 2, 5, 6  ;|
                                                         ;|
     DB  1, 2, 3, 4, 5, 6, 7, 8, 9,10, 2, 1, 4, 3, 6, 5  ;|
     DB  8, 7,10, 9, 5, 4, 4, 9, 4, 6, 2, 5, 5, 2        ;|
                                                         ;|
     DB  3, 5, 3, 6, 3, 7, 3, 8, 3, 9, 4, 5, 4, 6, 4, 7  ;|
     DB  4, 8, 4, 9, 5, 5, 5, 6, 5, 7, 5, 8, 5, 9        ;|
                                                         ;|
_@RNDmax EQU  $-_@RNDvet                                 ;| 
_@RNDcnt dw  _@RNDmax                                    ;| 
_@RNDptr dw  _@RNDvet                                    ;|
_@RNDadr dw  _@RNDvet                                    ;|
;---------------------------------------------------------*



;--------------------------------------------------------------------------*
; Dados para a funcao RAND - copiar p/fim segmento 'dados'                 |
;--------------------------------------------------------------------------*
_@RNDFLAG db     0  ; já gerou a semente para o LFSR                       |
_@RNDLFSR db     0  ; Linear Feedback Shift Register de 8 bits             |
_@RNDDEZ  db     10 ; divisor para obter restos de 0 a 9                   |
;--------------------------------------------------------------------------*


dados    ends

; definicao do segmento de pilha do programa
pilha    segment stack ; permite inicializacao automatica de SS:SP
         dw     128 dup(?)
pilha    ends
         
; definicao do segmento de codigo do programa
codigo   segment
inicio:  ; CS e IP sao inicializados com este endereco
         mov    ax, dados ; inicializa DS
         mov    ds, ax    ; com endereco do segmento DADOS
         mov    es, ax    ; idem em ES

;#################################################################
;###################### Escrita da Interface #####################
;#################################################################
		 	 
		mov     dh,24        ; linha 24
        mov     dl,79        ; coluna 79
        mov     ch,0         ; linha zero  
        mov     cl,0         ; coluna zero
        mov     bh,1Fh       ; atributo de preenchimento (fundo Azul e letras Brancas)
        mov     al,0         ; numero de linhas (zero = toda a janela)
        mov     ah,6         ; scroll window up
        int     10h          ; chamada BIOS (video)

		; mov     dh,23        ; linha 23
        ; mov     dl,44        ; coluna 44
        ; mov     ch,3         ; linha 3  
        ; mov     cl,4         ; coluna 4
        ; mov     bh,0Fh       ; atributo de preenchimento (fundo Preto e letras Brancas)
        ; mov     al,0         ; numero de linhas (zero = toda a janela)
        ; mov     ah,6         ; scroll window up
        ; int     10h          ; chamada BIOS (video)
		
		mov dh,0 
		mov dl,0
		call Posiciona_Cursor		
		
		lea dx,Interface
		call Escreve_String
		
		call Gera_Mina
		call Gera_Mina
		call Gera_Mina
		call Gera_Mina
		call Gera_Mina
		call Gera_Mina
		call Gera_Mina
		call Gera_Mina
		call Gera_Mina
		call Gera_Mina
		call Gera_Mina
		call Gera_Mina
		call Gera_Mina
		call Gera_Mina
		call Gera_Mina
		
		mov dh,CursorL 
		mov dl,CursorC
		call Posiciona_Cursor
		call Escreve_Cursor
		

;#################################################################
;##################### Interação com Usuario #####################
;#################################################################
	
Interacao_Usuario:
		call Foi_Teclado?
;Testa Esquerda
Testa_Esquerda:
		cmp ah,75
		jne Testa_Direita
		call Esquerda
;Testa Direita
Testa_Direita:
		cmp ah,077
		jne Testa_Cima
		call Direita		
;Testa Cima
Testa_Cima:
		cmp ah,072
		jne Testa_Baixo
		call Cima
;Testa Baixo
Testa_Baixo:
		cmp ah,080
		jne Testa_Tab
		call Baixo
;Testa Tab
Testa_Tab:
		cmp al,9
		jne Testa_Enter
		call Tab		
;Testa Enter			
Testa_Enter:		
		cmp al,13
		jne Nem_Foi	
		call Tecla_Enter
Nem_Foi:		
		jmp Interacao_Usuario
		jmp fim

;#################################################################
;#################### Rotinas Sem Parametros #####################
;#################################################################
Gera_Mina			proc
inicio_gera_mina:			
					mov TEMP_CORD,0
					call RAND
					mov TEMP_X, dl
					sub TEMP_X,1
					call RAND
					mov TEMP_Y, dl
					sub TEMP_Y,1
					mov al,TEMP_Y
					mul DEZ
					add TEMP_CORD,al
					mov ah,TEMP_X
					add TEMP_CORD,ah
					lea bx,Campo
					add bl,TEMP_CORD
					mov ah,[bx]
					cmp ah,16
					
					je inicio_gera_mina
					mov [bx],16
					cmp TEMP_CORD,0
					jne nao_caso1
					call caso1
					ret
nao_caso1:
					cmp TEMP_CORD,9
					jne nao_caso3
					call caso3
					ret
nao_caso3:
					cmp TEMP_CORD,90
					jne nao_caso4
					call caso4
					ret
nao_caso4:
					cmp TEMP_CORD,99
					jne nao_caso2
					call caso2
					ret
nao_caso2:
					mov ah,0
					mov al,TEMP_CORD
					div DEZ
					cmp ah,0
					jne nao_caso7
					call caso7
					ret
nao_caso7:					
					cmp ah,9
					jne nao_caso8
					call caso8
nao_caso8:
					cmp TEMP_CORD,9
					jg nao_caso5
					call caso5
					ret
nao_caso5:					
					cmp TEMP_CORD,90
					jl nao_caso6
					call caso6
					ret
nao_caso6:
					call caso9
					ret
					endp
					
caso1				proc
					lea bx,Campo
					add bx,1
					mov ah,[bx]
					cmp ah,16
					je caso1_1					
					mov ah, [bx]
					add ah,1
					mov [bx],ah					
caso1_1:					
					add bx,10
					mov ah,[bx]
					cmp ah,16
					je caso1_2
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso1_2:					
					sub bx,1
					mov ah,[bx]
					cmp ah,16
					je caso1_3
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso1_3:					
					ret
					endp

caso2				proc
					lea bx,Campo
					add bx,98
					mov ah,[bx]
					cmp ah,16
					je caso2_1
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso2_1:					
					sub bx,10
					mov ah,[bx]
					cmp ah,16
					je caso2_2
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso2_2:					
					add bx,1
					mov ah,[bx]
					cmp ah,16
					je caso2_3
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso2_3:					
					ret
					endp
					
caso3				proc
					lea bx,Campo
					add bx,8
					mov ah,[bx]
					cmp ah,16
					je caso3_1
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso3_1:					
					add bx,10
					mov ah,[bx]
					cmp ah,16
					je caso3_2
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso3_2:					
					add bx,1
					mov ah,[bx]
					cmp ah,16
					je caso3_3
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso3_3:					
					ret
					endp
					
caso4				proc
					lea bx,Campo
					add bx,91
					mov ah,[bx]
					cmp ah,16
					je caso4_1
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso4_1:					
					sub bx,10
					mov ah,[bx]
					cmp ah,16
					je caso4_2
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso4_2:					
					sub bx,1
					mov ah,[bx]
					cmp ah,16
					je caso4_3
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso4_3:					
					
					ret
					endp
					
caso5				proc
					lea bx,Campo
					add bl,TEMP_CORD
					sub bx,1
					mov ah,[bx]
					cmp ah,16
					je caso5_1
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso5_1:
					add bx, 10
					mov ah,[bx]
					cmp ah,16
					je caso5_2
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso5_2:					
					add bx,1
					mov ah,[bx]
					cmp ah,16
					je caso5_3
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso5_3:					
					add bx, 1
					mov ah,[bx]
					cmp ah,16
					je caso5_4
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso5_4:					
					sub bx,10
					mov ah,[bx]
					cmp ah,16
					je caso5_5
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso5_5:					
					ret
					endp
					
caso6				proc
					lea bx,Campo
					add bl,TEMP_CORD
					sub bx,1
					mov ah,[bx]
					cmp ah,16
					je caso6_1
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso6_1:
					sub bx,10
					mov ah,[bx]
					cmp ah,16
					je caso6_2
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso6_2:					
					add bx,1
					mov ah,[bx]
					cmp ah,16
					je caso6_3
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso6_3:					
					add bx,1
					mov ah,[bx]
					cmp ah,16
					je caso6_4
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso6_4:					
					add bx,10
					mov ah,[bx]
					cmp ah,16
					je caso6_5
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso6_5:					
					ret
					endp
					
caso7				proc
					lea bx,Campo
					add bl,TEMP_CORD
					sub bx,10
					mov ah,[bx]
					cmp ah,16
					je caso7_1
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso7_1:
					add bx,1
					mov ah,[bx]
					cmp ah,16
					je caso7_2
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso7_2:					
					add bx,10
					mov ah,[bx]
					cmp ah,16
					je caso7_3
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso7_3:					
					add bx,10
					mov ah,[bx]
					cmp ah,16
					je caso7_4
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso7_4:					
					sub bx,1
					mov ah,[bx]
					cmp ah,16
					je caso7_5
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso7_5:										
					ret
					endp
					
caso8				proc
					lea bx,Campo
					add bl,TEMP_CORD
					sub bx,10
					mov ah,[bx]
					cmp ah,16
					je caso8_1
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso8_1:
					sub bx,1
					mov ah,[bx]
					cmp ah,16
					je caso8_2
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso8_2:					
					add bx,10
					mov ah,[bx]
					cmp ah,16
					je caso8_3
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso8_3:					
					add bx,10
					mov ah,[bx]
					cmp ah,16
					je caso8_4
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso8_4:					
					add bx,1
					mov ah,[bx]
					cmp ah,16
					je caso8_5
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso8_5:										
					ret
					endp

caso9				proc
					lea bx,Campo
					add bl,TEMP_CORD
					sub bx,11
					mov ah,[bx]
					cmp ah,16
					je caso9_1
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso9_1:
					add bx,1
					mov ah,[bx]
					cmp ah,16
					je caso9_2
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso9_2:					
					add bx,1
					mov ah,[bx]
					cmp ah,16
					je caso9_3
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso9_3:					
					add bx,10
					mov ah,[bx]
					cmp ah,16
					je caso9_4
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso9_4:					
					add bx,10
					mov ah,[bx]
					cmp ah,16
					je caso9_5
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso9_5:																			
					sub bx,1
					mov ah,[bx]
					cmp ah,16
					je caso9_6
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso9_6:														
					sub bx,1
					mov ah,[bx]
					cmp ah,16
					je caso9_7
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso9_7:
					sub bx,10
					mov ah,[bx]
					cmp ah,16
					je caso9_8
					mov ah, [bx]
					add ah,1
					mov [bx],ah
caso9_8:					
					ret
					endp


					
					
Foi_Teclado?		proc
					mov al,00h
					mov ah,0h
					int 16h
					ret
					endp					

;Seta da Esquerda				
Esquerda			proc
					call Apaga_Cursor
					cmp CampoC,01
					jne Pode_Esquerda
					mov CampoC,10
					mov CursorC,42
					add Cursor_Real, 9
					mov dh,13
					mov dl,69
					call Posiciona_Cursor
					lea dx,_Dez
					call Escreve_String
					jmp Escreve_Esquerda
Pode_Esquerda:		
					sub CampoC,1
					sub CursorC,4
					sub Cursor_Real, 1
					mov dh,13
					mov dl,69
					call Posiciona_Cursor
					mov dl,'0'
					call Escreve_Char					
					mov dl,CampoC
					add dl,'0'
					call Escreve_Char
Escreve_Esquerda:	
					call Escreve_Cursor
					ret
					endp	
				
;Seta da Direita					
Direita				proc
					call Apaga_Cursor
					cmp CampoC,10
					jne Pode_Direita
					mov CampoC,1
					mov CursorC,6
					sub Cursor_Real, 9
					mov dh,13
					mov dl,69
					call Posiciona_Cursor
					lea dx,_Um
					call Escreve_String
					jmp Escreve_Direita
Pode_Direita:		
					add CampoC,1
					add CursorC,4
					add Cursor_Real, 1
					cmp CampoC,10
					jne Nao_eh_coluna_10
					mov dh,13
					mov dl,69
					call Posiciona_Cursor
					lea dx,_Dez
					call Escreve_String
					jmp Escreve_Direita
Nao_eh_Coluna_10:
					mov dh,13
					mov dl,69
					call Posiciona_Cursor
					mov dl,'0'
					call Escreve_Char					
					mov dl,CampoC
					add dl,'0'
					call Escreve_Char
Escreve_Direita:	
					call Escreve_Cursor
					ret
					endp

;Seta para Cima					
Cima				proc
					call Apaga_Cursor
					cmp CampoL,01
					jne Pode_Cima
					mov CampoL,10
					mov CursorL,22
					add Cursor_Real, 90
					mov dh,11
					mov dl,69
					call Posiciona_Cursor
					lea dx,_Dez
					call Escreve_String
					jmp Escreve_Cima
Pode_Cima:		
					sub CampoL,1
					sub CursorL,2
					sub Cursor_Real, 10
					mov dh,11
					mov dl,69
					call Posiciona_Cursor
					mov dl,'0'
					call Escreve_Char					
					mov dl,CampoL
					add dl,'0'
					call Escreve_Char
Escreve_Cima:	
					call Escreve_Cursor
					ret
					endp	

;Seta para Baixo
Baixo				proc
					call Apaga_Cursor
					cmp CampoL,10
					jne Pode_Baixo
					mov CampoL,1
					mov CursorL,4
					sub Cursor_Real, 90
					mov dh,11
					mov dl,69
					call Posiciona_Cursor
					lea dx,_Um
					call Escreve_String
					jmp Escreve_Baixo
Pode_Baixo:		
					add CampoL,1
					add CursorL,2
					add Cursor_Real, 10
					cmp CampoL,10
					jne Nao_eh_Linha_10
					mov dh,11
					mov dl,69
					call Posiciona_Cursor
					lea dx,_Dez
					call Escreve_String
					jmp Escreve_Baixo
Nao_eh_Linha_10:
					mov dh,11
					mov dl,69
					call Posiciona_Cursor
					mov dl,'0'
					call Escreve_Char					
					mov dl,CampoL
					add dl,'0'
					call Escreve_Char
Escreve_Baixo:	
					call Escreve_Cursor
					ret
					endp
;Tecla_Tab
Tab					proc
					cmp Status_Tab,'M'
					jne Status_era_A
					mov Status_Tab,'A'
					mov dh,15 
					mov dl,69
					call Posiciona_Cursor
					mov dl,'A'
					call Escreve_Char
					jmp Era_M_Mesmo
Status_era_A:
					mov Status_Tab,'M'
					mov dh,15 
					mov dl,69
					call Posiciona_Cursor
					mov dl,'M'
					call Escreve_Char					
Era_M_Mesmo:					
					mov dh,CursorL 
					mov dl,CursorC
					call Posiciona_Cursor
					ret
					endp
					
					
;Tecla_Enter
Tecla_Enter			proc
					cmp Status_Tab,'M'
					jne Abre
					call Marca_Pos
					ret
Abre:
					call Ler_Char
					cmp al,' '
					je Nao_Aberta
					cmp al,'M'
					je Nao_Aberta					
					ret
Nao_Aberta:			
					call Abre_Pos
					ret
					endp
					
					
Apaga_Cursor 		proc
					mov dh, CursorL
					mov dl, CursorC
					sub dl,1
					call Posiciona_Cursor					
					mov dl,' '
					call Escreve_Char
					mov dh, CursorL
					mov dl, CursorC
					add dl,1
					call Posiciona_Cursor					
					mov dl,' '
					call Escreve_Char
					ret
					endp
					
Escreve_Cursor		proc
					mov dh, CursorL
					mov dl, CursorC
					sub dl,1
					call Posiciona_Cursor					
					mov dl,'['
					call Escreve_Char
					mov dh, CursorL
					mov dl, CursorC
					add dl,1
					call Posiciona_Cursor					
					mov dl,']'
					call Escreve_Char
					mov dh, CursorL
					mov dl, CursorC
					call Posiciona_Cursor
					ret
					endp
					
					
					
Marca_Pos  			proc
					call Ler_Char
					cmp al, ' '
					jne Nao_era_espaco
					cmp Pra_Marcar, 0
					je Fim_Marca_Pos					
					mov dl,'M'
					call Escreve_Char	
					sub Pra_Marcar, 1
					call Atualiza_ContadorM
					mov dh,CursorL
					mov dl,CursorC
					call Posiciona_Cursor
					ret
Nao_era_espaco:		
					call Ler_Char
					cmp al, 'M'
					jne Fim_Marca_Pos
					mov dl,' '
					call Escreve_Char	
					add Pra_Marcar, 1
					call Atualiza_ContadorD
					mov dh,CursorL
					mov dl,CursorC
					call Posiciona_Cursor
Fim_Marca_Pos:
					ret
					endp


Abre_Pos			proc
					lea bx,Campo
					add bx,Cursor_Real
					mov dl,[bx]
					add dl,'0'
					cmp dl,'@'
					je Perdeu
					call Escreve_Char
					mov dh,CursorL
		            mov dl,CursorC
					call Posiciona_Cursor						
					sub Pra_Abrir, 1
					cmp Pra_Abrir, 0
					je Ganhou
					ret
Ganhou:
					call Vitoria
					ret
Perdeu:				
					call AbreCompleto
					
					call Derrota
					ret
					endp
					
Ler_Char			proc ;AL = Char lido
					mov ah, 08h
					mov bh, 0
					int 10h					
					ret
					endp
					
Atualiza_ContadorM  proc
					mov dh,5
					mov dl,76
					call Posiciona_Cursor
					call Ler_Char
					cmp al,'9'
					je 	era_9M
					mov dl,al
					add dl,1
					call Escreve_Char
					mov dh,6
					mov dl,76
					call Posiciona_Cursor
					call Ler_Char
					cmp al,'0'
					je era_10M
					mov dl,al
					sub dl,1
					call Escreve_Char
					ret
era_9M:
					mov dl,'0'
					call Escreve_Char
					mov dh,5
					mov dl,75
					call Posiciona_Cursor
					mov dl,'1'
					call Escreve_Char
					mov dh,6
					mov dl,76
					call Posiciona_Cursor
					call Ler_Char
					mov dl,al
					sub dl,1
					call Escreve_Char
					ret
era_10M:
					mov dl,'9'
					call Escreve_Char
					mov dh,6
					mov dl,75
					call Posiciona_Cursor
					mov dl,'0'
					call Escreve_Char
					ret					
					endp

Atualiza_ContadorD  proc	
					mov dh,5
					mov dl,76
					call Posiciona_Cursor
					call Ler_Char
					cmp al,'0'
					je 	era_10D
					mov dl,al
					sub dl,1
					call Escreve_Char
					mov dh,6
					mov dl,76
					call Posiciona_Cursor
					call Ler_Char
					cmp al,'9'
					je era_9D
					mov dl,al
					add dl,1
					call Escreve_Char
					ret			
era_10D:				
					mov dl,'9'
					call Escreve_Char
					mov dh,5
					mov dl,75
					call Posiciona_Cursor
					mov dl,'0'
					call Escreve_Char
					mov dh,6
					mov dl,76
					call Posiciona_Cursor
					call Ler_Char
					mov dl,al
					add dl,1
					call Escreve_Char
					ret	
era_9D:
					mov dl,'0'
					call Escreve_Char
					mov dh,6
					mov dl,75
					call Posiciona_Cursor
					mov dl,'1'
					call Escreve_Char
					ret					
					endp

Derrota				proc
					lea dx,_Perdeu
					call Escreve_MSG01
					call Retry?	
					ret
					endp

Vitoria				proc
					lea dx,_Ganhou
					call Escreve_MSG01
					call Retry?
					ret
					endp

Retry?				proc
					lea dx,_Retry?
					call Escreve_MSG02
Retry_Volta:					
					mov dh,23
					mov dl,65
					call Posiciona_Cursor
					mov ah,1
					int 21h
					mov dh,23
					mov dl,65
					call Posiciona_Cursor
					call Foi_Teclado?
					cmp al,13
					je Retry_Enter
					cmp al,8
					je Retry_Back
					jmp Retry_Volta
					
Retry_Enter:
					call Ler_Char
					cmp al,'s'
					je Retry_Sim
					cmp al,'S'
					je Retry_Sim
					cmp al,'n'
					je Retry_Nao
					cmp al,'N'
					je Retry_Nao
Retry_Back:					
					mov dl,' '
					call Escreve_Char
					Jmp Retry_Volta
Retry_Sim:
					call Prepara_Retry
Retry_Nao:
					jmp fim
					ret
					endp
					
Prepara_Retry		proc

					
					mov CursorC ,6
					mov CursorL ,4
					mov CampoL ,1
					mov CampoC ,1
					mov Cursor_Real ,0
					mov Status_Tab ,'M'
					mov Pra_Marcar ,15
					mov Pra_Abrir ,85
					mov Minas ,15
					jmp inicio
					ret
					endp
					
AbreCompleto		proc
					mov CursorC ,6
					mov CursorL ,4
					mov CampoL ,1
					mov CampoC ,1
					mov Cursor_Real ,0
					mov dh,CursorL 
					mov dl,CursorC
					call Posiciona_Cursor
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Baixo
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Baixo
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Baixo
					call Abre_Esp					
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Baixo
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Baixo
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Baixo
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Baixo
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Baixo
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Direita
					call Abre_Esp
					call Baixo
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Esquerda
					call Abre_Esp
					call Apaga_Cursor
					ret
					endp

Abre_Esp			proc
					lea bx,Campo
					add bx,Cursor_Real
					mov dl,[bx]
					add dl,'0'
					call Escreve_Char
					mov dh,CursorL
		            mov dl,CursorC
					call Posiciona_Cursor
					ret
					endp
					
					
;---------------------------------------------------------*
; Codigo da funcao RAND - copiar p/fim segmento 'codigo'  |
;---------------------------------------------------------*
; RAND     PROC                                            ;|
         ; push   ax                                       ;|
         ; push   bx                                       ;|
         ; mov    bx,_@RNDptr                              ;|
         ; mov    dl,[bx]                                  ;|
         ; inc    bx                                       ;|
         ; dec    _@RNDcnt                                 ;|
         ; jnz    naozero                                  ;|
         ; mov    ax,_@RNDmax                              ;|
         ; mov    _@RNDcnt,ax                              ;|
         ; mov    bx,_@RNDadr                              ;|
; naozero: mov    _@RNDptr,bx                              ;|
         ; pop    bx                                       ;|
         ; pop    ax                                       ;|
         ; ret                                             ;|
; RAND     ENDP                                            ;|
;---------------------------------------------------------*


;--------------------------------------------------------------------------*
; Codigo da funcao RAND - copiar p/fim segmento 'codigo'                   |
;--------------------------------------------------------------------------*
; Esta funcao simula o funcionamento de um Linear Feed-back Shift Register |
; (LFSR) de 8 bits, que e' um registrador que faz rotacao de 1 bit para a  |
; direita, construido usando 8 flip-flops tipo D e tres portas XOR de duas |
; entradas conforme mostrado abaixo:                                       |
;                                                                          |
; *---------------*--------*--------*------------------------------------* |
; |               |        |        |                                    | |
; |               |  *-*   |  *-*   |  *-*                               | |
; |               *->|X|   *->|X|   *->|X|                               | |
; |                  |O|-*    |O|-*    |O|-*                             | |
; |               *->|R| | *->|R| | *->|R| |                             | |
; |               |  *-* | |  *-* | |  *-* |                             | |
; |               |      | |      | |      |                             | |
; |               |  *---* |  *---* |  *---*                             | |
; |               |  |     |  |     |  |                                 | |
; |  *--*     *--*|  | *--*|  | *--*|  | *--*     *--*     *--*     *--* | |
; *->|  |---->|  |*  *>|  |*  *>|  |*  *>|  |---->|  |---->|  |---->|  |-* |
;    |b7|     |b6|     |b5|     |b4|     |b3|     |b2|     |b1|     |b0|   |
;    *--*     *--*     *--*     *--*     *--*     *--*     *--*     *--*   |
;                                                                          |
; Este LFSR gera uma sequencia 'pseudo-aleatoria' de valores entre 1 e 255,|
; passando apenas 1 vez por cada valor a cada 255 deslocamentos. Se a se-  |
; mente for zero, o registrador fica sempre com zero.                      |
;                                                                          |
;--------------------------------------------------------------------------*
; NOTA: devido 'a necessidade de dividir o valor do LFSR (1 a 255) por 10  |                                                                         
;       e usar o resto como valor de retorno, usando esta versao da RAND   |
;       nao se consegue acessar todas as posicoes do campo minado. Mas em  |
;       cada execucao do programa a sequencia de posicoes gerada e' outra. |
;--------------------------------------------------------------------------*
RAND     PROC                                                             ;|
         push   ax             ;                                          ;|
         push   bx             ; salva registradores na pilha             ;|
         push   cx             ;                                          ;|
         push   dx                                                        ;|
         cmp    _@RNDflag,255                                             ;|
         je     _@RNDja_tem_semente                                       ;|
_@RNDoutro:                                                               ;|
         mov    ah,0                                                      ;|
         int    1Ah            ; obtem numero de 'ticks' do relogio       ;|
         cmp    dl,0           ; para usar como 'semente' do LFSR         ;|
         je     _@RNDoutro     ; mas a semente do LFSR nao pode ser 0     ;|
         mov    _@RNDLFSR,dl   ; armazena a semente para o LFSR           ;|
         mov    _@RNDflag,255  ; indica que semente ja foi armazenada     ;|
_@RNDja_tem_semente:                                                      ;|
         mov    al,_@RNDLFSR   ; valor anterior do LFSR                   ;|
         ror    al,1           ; gira LFSR para a direita                 ;|
         jnc    _@RNDsem_xor   ; ajusta bits 3, 4 e 5, de acordo com o    ;|
         xor    al,00111000B   ;resultado dos XOR com bit 0               ;|
_@RNDsem_xor:                                                             ;|
         mov    _@RNDLFSR,al   ; guarda novo valor do LFSR                ;|
         mov    ah,0           ; como valor do LFSR varuia de 1 a 255,    ;|
         div    _@RNDDEZ       ; divide por 10 e usa o resto (0 a 9)      ;|
         pop    dx                                                        ;|
         inc    ah             ; incrementado de 1 unidade                ;|
         mov    dl,ah          ; como valor retornado por RAND no DL      ;|
         pop    cx             ;                                          ;|
         pop    bx             ; restaura registradores                   ;|
         pop    ax             ;                                          ;|
         ret                                                              ;|
RAND     ENDP                                                             ;|
;--------------------------------------------------------------------------*

		
;#################################################################
;#################### Rotinas Com Parametros #####################
;#################################################################


;Chamada Posiciona_Cursor
;		mov dh,Linha_da_tela
;		mov dl,Coluna_da_tela
;		call Posiciona_Cursor						
Posiciona_Cursor	proc
					mov bh,0
					mov ah,2
					int 10h
					ret
					endp

;Chamada Escreve_String
;		lea dx,Endereço_String
;		call Escreve_String	
Escreve_String		proc	
					mov ah,09h
					int 21h
					ret
					endp

;Chamada Escreve_Char
;		mov dl,Caractere
;		call Escreve_Char	
Escreve_Char		proc
					mov ah,2
					int 21h					
					ret
					endp
					

					
;Chamada Escreve_MSG01
;		lea dx,Endereço_String
;		call Escreve_MSG01	
Escreve_MSG01		proc
					mov Temp, dx
					mov dh,22
					mov dl,48
					call Posiciona_Cursor	
					mov dx, Temp
					call Escreve_String
					mov dh,CursorL 
					mov dl,CursorC
					call Posiciona_Cursor
					ret
					endp
					
;Chamada Escreve_MSG02
;		lea dx,Endereço_String
;		call Escreve_MSG02	
Escreve_MSG02		proc
					mov Temp, dx
					mov dh,23
					mov dl,48
					call Posiciona_Cursor
					mov dx, Temp
					call Escreve_String
					mov dh,CursorL 
					mov dl,CursorC
					call Posiciona_Cursor
					ret
					endp


					
					
;#################################################################
;############################# Final #############################
;#################################################################
fim:

		; mov     dh,24        ; linha 24
        ; mov     dl,79        ; coluna 79
        ; mov     ch,0         ; linha zero  
        ; mov     cl,0         ; coluna zero
        ; mov     bh,0Fh       ; atributo de preenchimento (fundo Preto e letras Cinza Claro)
        ; mov     al,0         ; numero de linhas (zero = toda a janela)
        ; mov     ah,6         ; scroll window up
        ; int     10h          ; chamada BIOS (video)
		; mov dh,0
		; mov dl,0
		; call Posiciona_Cursor						
	    mov    ax,4c00h           ; funcao retornar ao DOS no AH
        int    21h                ; chamada do DOS

codigo   ends

; a diretiva a seguir indica o fim do codigo fonte (ultima linha do arquivo)
; e informa que o programa deve começar a execucao no rotulo "inicio"
         end    inicio 
